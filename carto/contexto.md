# Contexto

El cordal desde Frailemendi hasta el nuevo cementerio de Urduliz es uno de los límites naturales entre los municipios de Urduliz y Sopela, sobre el que colindan múltiples parcelas, algunas de propiedad pública y otras de propiedad particular.
Desde la curva de Torre Barri hasta el nuevo cementerio, las parcelas al norte del cordal corresponden al municipio de Urduliz, mientras que las parcelas al sur del cordal corresponden al municipio de Sopela (excepto las más orientales),
tal como se observa en la {numref}`fig:catastro`:

(fig:catastro)=
```{figure} ../_static/img/Catastro.png
Parcelas de los municipios de Urduliz (azul) y Sopela (rojo) obtenidas del Catastro de Bizkaia y superpuestas sobre
la capa *Sattelite* de Google Maps.
```

Para favorecer la convivencia y la ordenación de actividades en el cordal, resulta necesario identificar la propiedad
(ayuntamientos, obispado, diputación, particulares, etc.) de las parcelas colindantes.
A mayores, con el fin de preservar la fauna y flora, resulta también necesario identificar qué especies protegidas se
encuentran y su ubicación.
Los Sistemas de Información Gerográfica (SIG), nos permiten superponer múltiples fuentes de información para identificar
los senderos y otras vías, los puntos de interés y las instalaciones, en conjunto con capas físicas o administrativas.

En esta sección se recoge información técnica para conocer la situación administrativa de cada elemento, así como toda
información que afecte al uso del suelo desde un punto de vista lúdico/deportivo.

## Acuerdos con propietarios particulares

Nótese que existen antecedentes cercanos de acuerdos entre administraciones y propietarios particulares para facilitar
el uso público de terrenos particulares.
Por ejemplo, en el caso del Ayuntamiento de Oñati y uno de los sectores incluidos en el proyecto *Escuelas de Escalada
de Gipuzkoa*, el [folleto técnico de Araotz](https://www.gmf.eus/galeria/Escuelas/2019/WEB%20ARAOTZ%20%281%29.pdf)
disponible en la página web de la *Federación Guipuzcoana de Montaña* incluye la siguiente nota:

> Jabetza pribatua Sarbide baimendua
>
> Sektore hauek, edo zati batzuk, jabetza pribatu batetan kokatuta daude.
> Bere titularrak Oñatiko Udalari utzi dizkio kirol-instalazio honen erabileraren kudeaketa eta bere sarbidea,
> eskaladaren praktika seguru, aske eta doakorako.
> Gure esker ona merezi du.
> Errespetatu jabetzan aurkitzen den guztia.

> Propiedad privada Acceso libre
>
> Estos sectores o una parte de los mismos, están ubicados en una propiedad privada.
> Su titular ha cedido al Ayuntamiento de Oñati la gestión del uso de esta instalación deportiva y su acceso, para la
> práctica segura, libre y gratuita de la escalada.
> Merece nuestro agradecimiento.
> Respeta todo aquello que se encuentra en la propiedad.

```{important}
Una vez identificados los propietarios y suscritos los consiguientes acuerdos, si procediera, sería recomendable hacer público un mapa (sea a través de la web del ayuntamiento o como un cartel) donde se indique en qué parcelas está
permitida la escalada y/o el senderismo y qué parcelas han de evitarse.
```
