(sig)=
# SIG

Los Sistemas de Información Geográfica (SIG), en inglés *Geographical Information System (GIS)*, son herramientas que
integran y relacionan diversos componentes para visualizar, analizar y/o modelar información geográficamente
referenciada.
Ver [w: Sistema de información geográfica](https://es.wikipedia.org/wiki/Sistema_de_informaci%C3%B3n_geogr%C3%A1fica).

## QGIS

Además de los múltiples visores online de propósito específico mencionados en este repositorio (OpenStreetMap, Google Maps/Earth, Wikiloc, Visor del SIGPAC, Visor del Catastro de Bizkaia, etc.) una de las herramientas SIG más
conocidas/utilizadas es [QGIS](https://www.qgis.org/).
Es software libre y gratuito, y está disponible para múltiples plataformas (GNU/Linux, Windows, macOS, etc.).

Utilizamos QGIS para combinar las múltiples fuentes de información, debido a que soporta múltiples formatos y
potencialmente permite mejorar el soporte (o solicitarlo).

Como base, además de la capa de OpenStreetMap disponible por defecto, añadimos *Googe Maps*, *Google Hybrid* y *Google
Satellite*, siguiendo las indicaciones de [Cómo añadir mapas base en QGIS (OpenStreetMap, Google, CARTO, Stamen,…) utilizando XYZ Tiles](https://mappinggis.com/2018/03/como-anadir-mapas-base-en-qgis-3-0-openstreetmap-google-carto-stamen/).

```{hint}
El fichero [sig/santa-marina.qgz](https://gitlab.com/urduliz/santa-marina/-/blob/main/sig/santa-marina.qgz) es el
proyecto de QGIS utilizado para obtener capturas como {numref}`fig:catastro` o {numref}`fig:tracks`.
```

## Capas

En el contexto que nos ocupa, nos interesa integrar y analizar en conjunto las siguientes capas de información:

- Distribución político-administrativa, entre municipios y entre propietarios de parcelas (ver {ref}`parcelas`).
  - Usos del suelo.
- Vías de comunicación (carreteras, pistas, paseos, senderos, etc.).
- Fauna y flora.
- Red fluvial.
- Topografía (curvas de nivel).

A continuación se indican las fuentes de las que disponemos para obtener la información de cada capa.

### Distribución político-administrativa

En principio, SIGPAC y el Catastro proveen información más que suficiente para esta capa.

Hay un plugin para QGIS que permite descargar la información de SIGPAC por municipio:

- [yt: QGIS-plugin SIGPAC Downloader by Fundación CETEMAS](https://www.youtube.com/watch?v=98nwmIAHLmQ)
- [gh: geomatico/qgis-sigpac](https://github.com/geomatico/qgis-sigpac)

Sin embargo, parece que sólo están disponibles los polígonos/trazos de los recintos, y no de las parcelas.

En el Catastro de Bizkaia podemos descargar la información por municipios en múltiples formatos:

- [appsec.ebizkaia.eus/O4GC000C/vistas/descargaParcelario.xhtml](https://appsec.ebizkaia.eus/O4GC000C/vistas/descargaParcelario.xhtml?language=es)

Descargando el Parcelario, primero en formato *SHAPE* y después en formato *CSV*, podemos relacionar en QGIS el campo
del código de parcela de la capa Parcela con el código de la tabla, de forma que se muestra el número en el mapa.
Así obtenemos un resultado similar a la visualización por defector del Visor gráfico de la web del Catastro de Bizkaia.

### Vías de comunicación

En principio, tanto OpenStreetMap como Google Maps (o Google Hybrid) permiten identificar correctamente las carreteras
y caminos asfaltados/hormigonados existentes.
De hecho, si se superponen las capas de SIGPAC o el Parcelario del Catastro, se observa que la mayoría de carreteras y
caminos son parcelas en sí, diferenciadas de los terrenos colindantes.

```{attention}
Nótese que la calle Goieta en Urduliz y los tres caminos/calles que suben de la calle Gatzarriñe en Sopela hacia el norte
entre Torre Barri y Moruatxa, atraviesan terrenos particulares sin que haya distinción parcelaria.
```

Con respecto a los senderos, ver la sección {ref}`tracks`.

#### Imprecisiones

Tanto Google Maps como OpenStreetMap tienen imprecisiones, algunas más graves que otras.

##### OpenStreetMap

En OpenStreetMap, el límite entre los municipios de Sopela y Urduliz está desplazado al norte (más o menos dependiendo
del punto del cordal), por lo que no se corresponde con la información del SIGPAC o del Catastro.
Sería interesante comunicarlo de alguna manera a los desarrolladores/mantenedores para que puedan corregirlo.

(viascomunicacion:imprecisiones:GoogleMaps)=
##### Google Maps

En Google Maps se muestran varias carreteras/caminos que hoy en día son inexistentes:

- En Urduliz, se muestra un camino que atraviesa la parcela 0038 desde el Parque de la Memoria hasta la calle Goieta a
  la altura de la parcela 0036.
  Actualmente, el camino con servidumbre de paso que se ha convertido en el Paseo de la Memoria va del Parking al Parque
  de la Memoria y finaliza ahí.
  La parcela 0038 está cercada prácticamente en su totalidad, ya que es un Hípica.
- En Sopela, se muestra que la pista/carretera que pasa del Parking de Urduliz hacia Sopela (al Oeste de Santa Marina)
  atraviesa la parcela 0127.
  Actualmente (y desde que tengo memoria), el camino gira en el límite noreste de la parcela 0127 y baja a la parcela
  0131, donde se encuentra la explanada que se usa como Parking de Sopela.
  Asimismo, el camino que sube de la calle Gatzarriñe pegada a la pared del cementerio finaliza al entrar en la parcela
  0134.
  El resto del trazado que recorre la parcela 0134 y después cruza 0135, 0137 y 0138, si existe hoy día, es parte del
  Garden Center.

### Fauna y flora

En el SIGPAC se puede obtener información sobre los usos, y en caso de que sean explotaciones agrícolas se indica el
tipo.
Sin embargo, por el momento no tenemos una fuente de información adecuada para superponer información sobre especies
protegidas o en peligro, como por ejemplo las aves que anidan por temporadas en diferentes puntos de cordal o una planta
que podría ser endémica del cordal.

### Red fluvial

Como se comenta en {ref}`parcelas`, el nacimiento del río Gobela podría encontrarse en las parcelas 0148, 0149 y/o 0151.
Pero no sabemos si esto es más una leyenda local.
Sabemos que en Urduliz el río Gobela está soterrado, y sale a la superficie entre las parcelas 1002 y 8001, pero sería
interesante contactar con alguien que tenga conocimientos de la red fluvial para saber si realmente hay un punto de nacimiento, o se conforma por filtración entre las peñas, la Campa y Marusas.

### Topografía

Dado el contexto, necesitaríamos una resolución de 3-5 m, que puede ser excesiva en comparación con los mapas utilizados
habitualmente en montaña.

Existe [opentopomap.org](https://opentopomap.org), pero la resolución es insuficiente (6-8 m) y parece impreciso
(por ejemplo, no está representada la cantera de Atxarte).

La Diputación Foral de Bizkaia tiene una página sobre [Cartografía y Ortofotos](https://www.bizkaia.eus/es/cartografia-y-ortofotos).
En el visor, se puede activar la *Cartografía Vectorial* para los municipios de Urduliz y Sopela.
Hay dos escalas disponibles: 1:5000 IGN y 1:500 DFB (escogemos la del Instituto Geográfico Nacional).
La minuta cartográfica que nos interesa es la 03754 que es la que cubre el cordal (ver {numref}`cartografia`).
La 03746 cubre el núcleo central del municipio de Urduliz.

(cartografia)=
```{figure} ../_static/img/Cartografia.png

Captura de la minuta cartográfica 03754, a través del visor web de la DFB.
```

La resolución parece suficiente y sí que se representan detalles como la cantera de Atxarte.

```{note}
Las minutas cartográficas pueden descargarse en formato PDF o DXF.
Todavía no he mirado cómo cargar esos formatos en QGIS.
```

(tracks)=
## Tracks

(fig:tracks)=
```{figure} ../_static/img/Tracks.png

Cuatro tracks (cyan, magenta, amarillo, naranja) superpuestos con el Parcelario de Urduliz (azul) y Sopela (rojo) sobre
Google Sattelite como base.
```

La fuente de información principal para identificar los senderos son los *tracks* de paseantes, senderistas y escaladores.
Hoy en día, prácticamente todos los *smartphone* incluyen GPS, por lo que no es necesario disponer de un dispositivo
específico (aunque sí recomendable en montaña por consumo/duración de la batería).
Es por tanto habitual que se registren los recorridos, típicamente acompañados de puntos de interés y fotografías, y se
compartan a través de páginas/aplicaciones como [Wikiloc.com](https://www.wikiloc.com/) o [Strava](https://www.strava.com/).

[opentracksapp.com](https://opentracksapp.com/) es una aplicación libre y gratuita par Android que permite registrar
recorridos y exportarlos en múltiples formatos.
No incluye mapas por defecto, pero puede complementarse con OpenStreetMap para poder visualizar los recorridos sobre una
base.

El formato más utilizado para compartir este tipo de información es GPX.
Sin embargo, al descargar un track de Wikiloc en formato GPX, o al exportarlo de OpenTracksApp, y después importarlo
en QGIS, se guardan el recorrido y los puntos de interés, pero no se preservan los iconos o las fotografías.
Sí se guardan los nombres de los puntos de interés, por lo que se puede identificar el propósito de su marcado;
pero no es tan cómodo identificar el punto en el que se ha obtenido una fotografía como lo es en Wikiloc o en
OpenTracksApp.

Por lo tanto, es útil/necesario importar los tracks en QGIS utilizando el formato GPX, ya que nos permite superponer
las capas del Parcelario y así saber qué sectores de escalada o tramos del cordal se encuentran en propiedad privada.
No obstante, sería muy útil averiguar si es posible preservar los iconos y/o fotografías en QGIS.

```{note}
OpenTracksApp permite exportar en formato KMZ (con fotos) o KML (sin fotos).
Sin embargo, parece no ser del todo compatible como el soporte de KMZ/KML en QGIS o Google Maps/Earth.
Sería interesante analizar un poco mejor cuál es el problema.
```

```{note}
Wikiloc permite exportar, además de GPX o TCX, *'En formato KML de Google Earth para visualizar en 3D'*.
Todavía no he probado a importar ficheros KML exportados de Wikiloc en QGIS o Google Maps/Earth.
```
