(parcelas)=
# Parcelas

El Sistema de Información Geográfica de Parcelas Agrícolas (SIGPAC) del Ministerio de Agricultura, Pesca y Alimentación
permite mostrar las parcelas/recintos y los usos.
Ver, por ejemplo, la {numref}`sigpac`.
Resulta, por lo tanto, útil para para conocer las actividades que se realizan o pueden realizar, pero no permite conocer
la propiedad.

(sigpac)=
```{figure} ../_static/img/SigPac_EPSG3857.jpg

Captura SigPac_EPSG3857 descargada del [Visor SIGPAC](https://sigpac.mapa.gob.es/fega/visor/), que muestra las parcelas
a ambos lados del cordal, desde la curva de Torre Barri al Oeste hasta el Haurren Basoa en el Este.
```

Por suerte, a través del visor gráfico del catastro de Bizkaia ([bizkaia.eus/es/catastro-de-bizkaia](https://www.bizkaia.eus/es/catastro-de-bizkaia)),
pueden obtenerse los trazos que identifican las mismas parcelas y además se indica el identificador concreto de cada una.
En esta sección se usan dichos identificadores para ordenar la información de la que disponemos sobre ellas.
Son las indicadas a continuación, de Oeste a Este, Norte primero y Sur después.

```{note}
El prefijo de las parcelas en el municipio de Urduliz es 089 0001, y el de las parcelas en el municipio de Sopela es
085 0005.
```

```{note}
Las parcelas 0002, 0001, 0004, y 0005, situadas al sur del cordal, en el extremo este, corresponden al municipio de Urduliz.
El resto de parcelas al sur del cordal corresponden al municipio de Sopela.
```

## Urduliz

(parcelas:0153)=
### 0153

Pertenece a la casa de la parcela 0154.
Las dos parcelas están divididas por el camino Torre Barri.

El 2023/08/09 pasé por el camino.
Al subir, había un hombre de unos 45 años cortando la hierba con una desbrozadora.
Al bajar, 10 minutos después, la había apagado y estaba descansando, por lo que aproveché para hablar con él.
Me confirmó que las parcelas a ambos lados del terreno pertenecen a la misma casa; y que aunque tienen una parte cercada
para los caballos, la parcela llega hasta la cresta.
Me indicó también que quieren retirar el eucalipto que hay entre el cercado de los caballos y la cresta para reemplazarlo
por especies autóctonas.
Así, el tramo de camino/sendero/pista que va de la curva de Torre Barri hacia el E lo están limpiando ellos.
Por su ánimo, me he animado a preguntarle que le/les parece que haya senderistas/paseantes recorriendo esa linde de su parcela.
Una de las primeras ideas que le ha venido a la cabeza ha sido la suciedad; pero conforme hemos seguido la conversación
se ha mostrado muy contento con la posibilidad de que haya un recorrido que la gente pueda hacer desde Frailemendi hasta el nuevo cementerio por la cresta.
Ha mencionado también que hay alguna trinchera en ese tramo, y que lo estuvieron mirando en su día.
Supongo que los investigadores o la Diputación no lo consideraría reseñable para incluirlo en el Itinerario de la Memoria, o bien lo dejaron fuera por estar en terreno particular.
Al mencionarle que iba a intentar ir desde la curva de Torre Barri hasta el Parking de Santa Marina (con botas y
pantalones largos), me avisó de que sólo han limpiado el camino hasta la mitad de su parcela más o menos.
Le he indicado que el otro día intenté hacerlo en sentido contrario, y que más o menos he visto el nivel de la vegetación
y la transitabilidad.
A diferencia de las parcelas en el lado de Sopela (0121 y 0126), esta parcela es probablemente irrelevante desde el punto
de vista de la escalada (salvo algún pequeño bloque/travesía que sí pudiera haber), pero es muy relevante para la
(re)habilitación del sendero por la cresta.

### 0151

Podría contener el nacimiento del río Gobela.

### 0149

Contiene parte del posible sector Parking.

Podría contener el nacimiento del río Gobela.

Contiene una vivienda en el extremo norte de la finca, el opuesto a la pared norte del cordal (que recorre la linde sur de la finca).

### 0148

Contiene parte del posible sector Parking.

Podría contener el nacimiento del río Gobela.

Contiene una vivienda en la mitad norte de la finca, el lado opuesto a la pared norte del cordal (que recorre la linde sur de la finca).

Parece ser que la linde con las parcelas del aparcamiento sufrió un ajuste que la hizo más perpendicular al cordal,
mientras que anteriormente seguía era perpendicular a la inclinación del terreno.

### 0081 y 0082

Las parcelas están divididas por un camino a ninguna parte, que termina en la linde con la parcela 0148.

Contiene el aparcamiento, y parte del posible sector Parking.

En principio, estas parcelas son propiedad del Ayuntamiento de Urduliz.

¿Es posible que la razón por la que se derrumbara parte de este sector sean los eucaliptus (que secan la tierra)?

### 0079

Contiene el sector de escalada Santa Marina N y Santa Marina Escaleras.

Conversando con un vecino del barrio, me ha hecho saber que hasta hace unos años, un terreno particular llegaba hasta la base de la pared N de Santa Marina.
La parcela 0079 pertenecía a la casa de la parcela 0064.
Sin embargo, el paseo que ahora va al Parque de la Memoria tenía servidumbre de paso (para el acceso a la parcela 0038 cuando la calle Goieta no existía).
Por eso nunca estuvo vallado el terreno entre el camino/pista/paseo y la pared (no habría tenido sentido vallar un recinto de 2-3 m x 50 m).
De esto concluyo que todas las décadas pasadas hemos estado escalando en terreno particular, creyendo que era público.

Hace unos años, se expropió el recinto entre el paso/pista/camino y la pared, y se construyeron el paseo y el Parque de la Memoria.
Por lo tanto, en principio esta parcela es actualmente propiedad del Ayuntamiento de Urduliz.

### 0062 y 0063

Las parcelas están divididas por el paseo al Parque de la Memoria.

La parcela 0062 contiene la base del sector Moruatxa N Bajo, aunque no la pared.
A diferencia de otras parcelas, no llega hasta la cresta del cordal.

Según Koldo Zuazo, son de propiedad privada.

Sin embargo, según Gorka, ese terreno era/es suyo.
El tramo desde el paseo hasta el cordal se lo vendió al Ayuntamiento.
Y el pico del Norte lo conmutó por otro trocito de terreno al Este, pero de palabra (ni siquiera tiene claro cuáles son).

```{note}
Sería interesante valorar si el Ayuntamiento de Urduliz podría comprar la parcela 0063 (o ambas si la 0062 no está
comprada ya) y después utilizar 0063 para acordar un reajuste de lindes con las parcelas 0218 y 0059:
- Ceder la mitad superior de la parcela 0063 (63_1 y 62_3) a la vivienda de la parcela 0218 a cambio de mover la linde
  Este tantos metros como fuera necesario para igualar el área.
- Mover la linde Sur de la parcela 0059 para compensar el área ganada del cambio anterior (a su Oeste).
  - Como resultado, la calle Goieta pasaría a estar en la parcela 0060 de propiedad municipal (que se vería incrementada
  en compensación por el área cedida, 63_1 y 63_3); eliminando el conflicto existente para acceder el Parque de la
  Memoria a la calle Goieta.

El resultado de la commutación es que Gorka dejaría de tener los metros correspondientes al pico en ese otro sitio.
Al hacer el ajuste, el vecino que actualmente usa el pico pasaría a ser propietario del mismo, por lo que Gorka estaría
recibiendo el pago por "su parcela menos el pico y esos metros de más en otro sitio".
```

```{important}
Si no se hubiera comprado la parcela 0062 y no fuera posible que el Ayuntamiento de Urduliz la adquiera, debería
suscribirse un acuerdo similar al del Ayuntamiento de Oñati para permitir la práctica de la escalada en Moruatxa N Bajo.
```

### 0229

Contiene los sectores Moruatxa N Alto, Moruatxa Arista, Pino Bajo y Pino Alto.

Aunque en el Ayuntamiento de Urduliz parecen no conocer la titularidad de esta parcela, el prefijo es el mismo que el
resto de parcelas de Urduliz.
¿Es posible que sea municipal?
Resultaría sin duda curioso, ya que tradicionalmente se ha considerado Santa Marina la peña de Urduliz y Moruatxa la
peña de Sopela (hay un mojón en la cima).
En detalle, parece que la linde sur de esta parcela recorre la arista de Moruatxa, por lo que el mojón de la cima está
justo en la linde, y Moruatxa S está en el municpio de Sopela.

### 0061 y 0060

Parque de la Memoria y acceso a los sectores Pino Bajo, Pino Alto y Moruatxa Arista.

En principio, son propiedad del Ayuntamiento de Urduliz.
Desconozco si se adquirieron cuando se hizo el Parque de la Memoria, o ya eran de propiedad municipal.

Tiro al plato.

(parcelas:0059)=
### 0059

Propiedad privada. Ugarte.

Hay un tramo de pocos metros entre la calle Goieta y el Parque de la Memoria que no pertenece al Ayuntamiento, sino a esta parcela.
Aunque los vecinos y paseantes lo utilizan (por eso está más o menos abierto dependiendo de la época del año), esto
explica por qué el Itinerario de la Memoria no indica ese paso.

```{note}
El propietario de la finca 0038 está dispuesto a ceder 1 m x 15 m de su linde Oeste en caso de que el propietario de esta parcela estuviera dispuesto a ceder un área similar, de forma que pueda hacerse un acceso desde la parcela 0060 a la calle Goieta por el extremo Noreste.

Ver también nota sobre la parcela 0063.
```

```{note}
El 7 de septiembre hablé con Ugarte sobre las aguas, manantiales y senderos (ver {ref}`urdunlaitz`).
Aproveché para preguntarle también sobre el acceso del Parque de la Memoria a la calle Goieta.

La razón de tener el terreno sin cortar es que cambiaron las leyes sobre la quema de poda y lo que
antes podía hacer sin mucho esfuerzo ni planificación se convirtió en tedioso.
Me comentó que su principal molestia es que el uso haga derecho.
Es decir, que en virtud de permitir explícitamente que los senderistas/paseantes vecinos hagan ese atajo, dentro de unos
años se considere que eso es un paso tradicional y por lo tanto se le acabe quitando ese terreno por la cara.

En cierta manera, ya ha sucedido con la calle Goieta en sí.
Dependiendo del mapa que se observe, parece que lo que pudo haber en el trazado de la calle Goieta al cruzar por la
parcela 0059 fue una sendero para ganado, pero nada cercano a una calzada.
También dependiendo qué mapa se mire, el paseo de la memoria (que era una servidumbre de paso) cruzaba en diagonal por
fuera de la parcela 0059, atravesando lo que hoy en día es la Hípica de la parcela 0038;
como si la rampa de acceso a la Hípica fuera en su día la pista/calzada.
Ver por ejemplo, Google Maps:

![](../_static/img/IturriagaEtenZaldiene.png)

Entiendo que si ya ha sucedido que un sendero menor se haga carretera, mientras otros vecinos conservan sus parcelas
íntegras, haya cierta reticencia a que pueda volver a suceder.

Por otro lado, me dijo que no tiene problema en vender la parte que queda de la carretera hacia arriba.
No tiene particular interés en mantener su parcela íntegra, o rectangular.
En su día el Ayuntamiento midió el trozo y le hicieron una oferta, pero no llegaron a un acuerdo.
Así como otros vecinos vendieron algunos terrenos a precio de catastro (que es realmente irrisorio porque se presupone
que poco uso se puede dar a esos suelos rurales), Ugarte considera que debe pagarse un precio razonable para el
uso/disfrute que se va a hacer.
Me parece una postura muy lícita.

Por lo tanto, parece descortés proponerle que el ayuntamiento corte y mantenga esa parte del terreno (junto con el Parque
de la Memoria) sin un acuerdo que diga muy explícitamente que es un terreno particular cedido para disfrute público y que
el propietario puede rescindir el acuerdo unilateralmente.
Idealmente, si el Ayuntamiento tiene presupuesto para seguir adquiriendo terrenos a lo largo del cordal, sería
interesante hacer conmutaciones como se explica en la nota sobre la parcela 0063, para que realmente no se "quite" más
terreno, ya que la parcela 0059 suficiente está contribuyendo al bien público teniendo una carretera atravesándola
(no por la linde).
```

(parcelas:0038)=
### 0038 y 0036

Propiedad privada.

Contiene los posibles sectores Erdikoatxa N y parte de Lukiatxa Mirador.

Contiene una Hípica (de ahí el nombre del sector que se encuentra en la parcela contigua).

El propietario es Koldo Zuazo, que ha estado y está muy involucrado en las investigaciones y los yacimientos en el municipio, así como en la recuperación y mantenimiento del Itinerario de la Memoria.

```{note}
El propietario está dispuesto a ceder parte de su linde Oeste para hacer un acceso de la parcela 0060 a la calle Goieta.

Ver nota sobre la parcela 0059.
```

### 0035

Contiene una vivienda/granja en la mitad norte de la finca, el lado opuesto a la pared norte del cordal (que recorre la linde sur de la finca).

### 0034 y 0033

Contiene el posible sector Lukiatxa nido, así cómo el bloque desenterrado.

Propiedad privada.

Contiene un nido de ametralladora en el extremo norte, pero el propietario no consintió su preparación e inclusión en el Itinerario de la Memoria.

```{important}
Para permitir la escalada, debería confirmarse por GPS que Lukiatxa nido y el bloque desenterrado se encuentran en esta parcela, y en su caso suscribirse un acuerdo similar al del Ayuntamiento de Oñati.
En su defecto, prohibir la escalada en esta parcela.
```

(parcelas:0032)=
### 0032, 0031, y 0030

Ayuntamiento de Urduliz.

### 0027

### 0202

### 0014

Contiene una vivienda en el extremo noreste de la finca, el lado opuesto al cordal (que recorre la linde sur de la finca).

### 0013

### 0012

### 0007

### 0251

### 0006

Algunos vecinos están limpiando el sendero por el cordal de Este a Oeste, empezando en las parcelas 0005 y 0006 (por el lateral sur del cementerio).

En principio se puede llegar hasta la cantera Atxarte, que es un posible sector.
Creo que Atxarte está a la altura de la parcela 0013, pero no lo he confirmado in-situ todavía.

### 0002

### 0001

### 0004

### 0005

## Sopela

(parcelas:0121)=
### 0121 y 0126

El 2023/08/08 hablé muy brevemente con un señor (a ojo, de más de 75 años) que estaba en la huerta de la parcela 0121.
Me confirmó que esa parcela pertenece a la casa de la parcela contigua hacia el este (0126), mientras que del camino
hacia abajo es de otro vecino.
Mirando el SigPac y por la densidad de la vegetación, interpreto que el "pico" de la parte SE de su parcela, lo que queda
al sur del camino, no lo utilizan.
No quise alargar la conversación porque estábamos hablando a través de un seto, y el señor me pareció un poco
desorientado con mis preguntas.
Fue una tarde calurosa, estaba atendiendo su huerta, le hablé desde su espalda (yo en el camino), al girarse el sol
quedaba a mi espalda por lo que le daba en la cara...
No obstante, me respondió amablemente a las 2-3 preguntas que le hice.
En lo que respecta a la escalada, creo que no merece la pena siquiera plantearlo hasta que por su propia voluntad decidan
dejar de trabajar ese trozo de parcela.
Actualmente tienen las hortalizas demasiado cerca de la pared como para que resulte mínimamente razonable plantear una
base escalable de 2 m.
Además, eso pertenece al municipio de Sopela.
Para la rehabilitación del sendero/pista es irrelevante esta parcela, ya que hay una pared de 30 m que hace la
delimitación natural clara y relativamente aislante acústicamente (no así visualmente).

(parcelas:0430)=
### 0430

(parcelas:0127)=
### 0127

(parcelas:0131)=
### 0131

### 0134

### 0135

### 0140

(parcelas:0281)=
### 0281

La parcela de Sopela con roble americano que está a la altura de Erdikoatxa parece colindar con la Hípica de la parcela
de Urduliz sólo en la mitad de su extensión.
Hay una parcela triangular (0229, donde se encuentran Moruatxa y la Arista) que cubre la otra mitad de la linde.
Por lo tanto, puede que no pueda recomendarse un itinerario que recorra Erdikoatxa por el sur, pero sí uno que vaya
exactamente por la cresta.

### 0147

### 0148

### 0151

### 0150

### 0221

### 0160

### 0162

### 0164

## Registro de la propiedad

En la sección *'Publicidad del Registro'* de la página [registradores.org/el-colegio/registro-de-la-propiedad](https://www.registradores.org/el-colegio/registro-de-la-propiedad),
se indica lo siguiente:

> Interés legítimo. Los Registros son públicos para quienes tengan interés conocido en averiguar el estado de los bienes
> inmuebles o derechos reales inscritos. El interés se presume en toda autoridad, empleado o funcionario público que
> actúe por razón de su cargo u oficio.

Por lo tanto, parece necesario solicitar al ayuntamiento que consulte la propiedad de todas las parcelas anteriores,
ya que para particulares el servicio de consulta parece ser de pago.
