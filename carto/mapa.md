(mapa)=
# Mapa interactivo

La [sección sobre El Cinturón de Hierro en Urduliz](https://www.urduliz.eus/es-ES/turismo/Lugares-Interes/Paginas/ElCinturondeHierroenUrduliz.aspx) de la página web del Ayuntamiento de Urduliz incluye un enlace a un [Mapa interactivo
del Itinerario de la Memoria](https://www.google.com/maps/d/u/0/viewer?mid=17aKmSl67AfaC_MdSuRZBm9vzggC2Yolc&amp;ll=43.37449990056062%2C-2.955430999999984&amp;z=16) realizado con *Google My Maps*.

El siguiente [Mapa interactivo de las Peñas de Santa Marina](https://www.google.com/maps/d/u/0/edit?mid=1FDLi1N6q_EEZYtcmNaSPP46NyRDjFEE&usp=sharing)
es una copia del anterior, al que se le han añadido capas.
En esta sección se explican las capas adicionales.

```{note}
*Google My Maps* es una herramienta que permite a los usuarios crear capas sobre los mapas base de *Googler Maps* y
*Google Earth*.
Cualquier puede, con una cuenta de *Google*, crear una copia de cualquier de estos mapas y añadir otras capas.
```

```{important}
Debido a que existen multiples formatos para guardar y transmitir información geográfica y tracks, las capas mostradas
en el mapa interactivo basado en *Google My Maps* tienen sólo parte de la información de la que disponemos;
aquella que hemos obtenido en un formato compatible con la aplicación de *Google*.
En la sección {ref}`sig` se explican otras fuentes de información y otras herramientas/aplicaciones.
```

```{attention}
Gracias a la contribución de Aitor Oriñuela Salaverria, estamos realizando una versión del mapa interactivo utilizando
[Leaflet](https://leafletjs.com/), una librería JavaScript open source que nos permite añadir capas a medida e incluirlo
en cualquier página web.

El prototipo está disponible en [urduliz.gitlab.io/santa-marina/leaflet](../leaflet){.external}.
```

## Sectores de escalada

En esta capa se indican los sectores de escalada conocidos/documentados y los que están sin (re)equipar y sin croquis.
La leyenda es la siguiente:

* Símbolo de escalada verde:
  sector conocido, con inicio de vías a nivel del suelo (entendiendo las escaleras también como suelo por estar
  protegidas por barandillas).
  Moruatxa N Bajo es verde porque se explica en este documento y hay ciertos equipadores locales que lo conocen, pero no
  figura en los croquis.

* Símbolo de escalada amarillo:
  sector conocido, con inicio de vías en altura y parcialmente expuestas.
  Es una "escuela" de escalada, por lo que las repisas son suficientemente anchas y hay cables a modo de pasamanos donde
  los aseguradores pueden anclarse.
  Pero considero relevante señalar esta diferencia con respecto a los otros sectores, ya que en éstos la masificación
  puede ser peligrosa.

* Símbolo de escalada naranja:
  sector conocido pero peligroso.
  Concretamente, se usa en el sector Hípica por posibles desprendimientos.

* Símbolo de escalda gris:
  (posible) sector de escalada sin (re)equipar.
  Algunos como Lukiatxa Nido o Lukiatxa Mirador, tienen chapas antiguas; el del Parking tiene una sola chapa; otros no
  tienen nada.

* Punto verde:
  bloques/travesías a nivel del suelo (menos de 3 m de altura), que no requieren ningún material salvo una colchoneta,
  si acaso (la mayoría son tan a nivel de suelo que la caída es de apenas 1 m).
  La única excepción es Santa Marina Tejado, que se refiere a 1-2 vías que suben desde la puerta trasera de la iglesia
  hasta la ikurriña.
  Se pueden hacer como bloque con cierta valentía, pero considero muy recomendable que estén debidamente equipadas.
  No aparecen en los croquis.

* Punto amarillo:
  travesía de una arista.
  En principio, sólo hay uno que indica el paso de Moruatxa O hasta el E de la Arista, que es una travesía equipada
  incluyendo un ascenso de III/III+ y un posible rápel, independientemente del sentido en que se haga.
  En otras palabras, no es para senderistas ni para escaladores de deportiva, sino para quienes quieren aprender a
  progresar por terreno expuesto.
  Por otro lado, es muy interesante tener un recorrido así en la escuela.

## Tracks GPX

Se muestran varios tracks (y los correspondientes *waypoints* o puntos de interes), cada uno en una capa.

Estos nos permiten conocer cuáles son los senderos transitables (con mayor o menor facilidad) de cara a planificar
las tareas de poda y jardinería para el mantenimiento del zarzal y la regeneración del bosque autóctono que se está
llevando a cabo en varias parcelas (de propiedad tanto pública como privada).

Estos tracks están importados en formato GPX, por lo que no tienen toda la información disponible en las fuentes.
Ver la sección {ref}`sig`.
