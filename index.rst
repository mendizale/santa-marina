Peñas de Santa Marina en Urduliz-Sopela
#######################################

Este repositorio contiene información pasada, presente y futura sobre las Peñas de Santa Marina situadas entre
Urduliz y Sopela en Bizkaia;
en lo que respecta a la práctica de la escalada deportiva/libre, el senderismo/trekking y otras actividades/eventos de
ocio.

.. NOTE::
  La información aquí contenida ha sido reunida y ordenada por un particular, vecino de Urduliz; por lo que puede haber
  incorrecciones y no representa la opinión/postura de ninguna entidad o colectivo.
  Si encuentras algún error o dispones de información adicional, por favor, comunícala a través de
  `gitlab.com/urduliz/santa-marina/-/issues <https://gitlab.com/urduliz/santa-marina/-/issues>`__.

.. IMPORTANT::
  Las fotografías, mapas y croquis de este repositorio tienen múltiples orígenes y el autor del sitio no ostenta el
  copyright de todas ellas.
  Algunas han sido facilitadas por otros vecinos o habituales de la zona, otras se han descargado de páginas como
  Wikiloc, de las Diputaciones, Ayuntamientos o blogs.


.. image:: _static/img/SigPac_Home.jpg
   :align: center


.. toctree::
  :hidden:

  referencias


.. toctree::
  :caption: Escalada
  :hidden:

  escalada/sectores-equipados
  escalada/sectores-sin-equipar
  escalada/croquis
  escalada/equipadores
  escalada/eskalada-eguna
  escalada/rocodromos


.. toctree::
  :caption: Senderismo
  :hidden:

  senderismo/cordal
  senderismo/itinerario
  senderismo/arroyo
  senderismo/historias
  senderismo/fuentes


.. toctree::
  :caption: Instituciones
  :hidden:

  instituciones/responsabilidad
  instituciones/ayuntamientos
  instituciones/diputacion
  instituciones/gogora


.. toctree::
  :caption: Cartografía
  :hidden:

  carto/contexto
  carto/mapa
  carto/parcelas
  carto/sig
