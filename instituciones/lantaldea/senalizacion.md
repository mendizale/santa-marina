# Consulta presupuesto señalización

Tenemos que definir qué señales y carteles concretos queremos solicitar que se pongan en el año 2024 y requerir que se
consigne una cantidad en los presupuestos.

Por el momento, una decena de carteles, un poste y uno o dos paneles verticales.

## Escalada

- Retirar los tres paneles del aparcamiento (el de la diputación sobre escalada, el del itinerario de la memoria que
  está pintado, y el de los croquis de LME que esta roto), y reemplazarlos por uno sólo vertical situado con un poco de
  sentido de forma que pueda leerse desde ambos lados.
  Si es necesario, poner dos paneles para tener espacio para cuatro láminas (en vez de dos).
  - En la lámina donde se indiquen los sectores, deberíamos indicar también los sectores conocidos pero donde no se debe
    escalar (por tratarse de propiedad privada y no tener permiso, o por seguridad).

- Mover el poste que señala la subida del Itinerio de la Memoria al nido de la puerta trasera de la Iglesia.
  - Opción A: ponerlo pocos metros más al Este (donde ha estado la subida "toda la vida").
  - Opción B: ponerlo varios metros más al Este, a la altura de la base de Moruatxa, de forma que la subida sea más
    tendida en vez de tan directa.
      - Esto depende de la confirmación de que la parcela 0062 fue adquirida por el Ayuntamiento cuando se adquirieron
        las parcelas 0079 y 0062.

- Añadir dos carteles al poste anterior:
  - Hacia el Sur, sectores Santa Marina Sur (Ikurriña), Moruatxa Sur y Moruatxa Norte Alto.
  - Hacia el Este, sectores Pino Bajo, Pino Alto y Moruatxa-Arista Sur.

- Añadir tres carteles en la pared Oeste del depósito:
  - Por el Norte del depósito al Este, sector Moruatxa Norte Alto y la Arista.
  - Por el Norte del depósito bajando hacia el Norte, sectores Santa Marina Norte, Moruatxa Norte Bajo, Pino Bajo,
    Pino Alto y Moruatxa-Arista Sur.
  - Por el Sur del depósito, al Este Moruatxa Sur, y al Sur-Oeste Santa Marina Sur (Ikurriña).
  - El depósito está en la parcela 0131 de Sopela, por lo que hay que pedir permiso al Ayuntamiento de Sopela.

- Poner un poste en el camino superior del Parque de la Memoria, entre la entrada a los senderos Pino Bajo y Pino Alto.
  Añadir dos carteles:
  - Hacia el Sur-Oeste (derecha mirando del camino al cordal): Pino Bajo.
  - Hacia el Sur-Este (izquierda mirando del camino al cordal): Pino Alto y Moruatxa-Arista Sur.

- Añadir un cartel al poste en la calle Goieta, poco antes del Haurren Basoa, indicando el acceso a Lukiatxa como sector
  de bloque.
  - Tendríamos que confirmar con los propietarios de las parcelas 0034, 0035 y 0036, que no tienen problema en que la
    gente practique bloque ahí, y suscribir un acuerdo cuando sepamos cómo hacerlo.

- Añadir un cartel en el poste de la cresta de Lukiatxa (donde se indican dos nidos a un lado y uno a otro), indicando
  el sector de bloque hacia el Oeste (hacia el mirador y el pasillo).

### Vías

*TBC*

## Árboles

*TBC*

## Senderismo

*TBC*
