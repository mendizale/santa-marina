# Consulta propietarios parcelas

El grupo de trabajo Santamariñe solicita al Ayuntamiento de Urduliz que consulte por los medios a su disposición y como
entidad legítimamente interesada cuáles son los propietarios de las parcelas listadas al final de este documento.

De cara a proponer acciones de mantenimiento y mejora para el año 2024 (y posteriores), en lo que respecta a senderos y
recorridos de paseo, el grupo de trabajo Santamariñe necesita conocer qué parcelas son de propiedad particular y cuáles
de propiedad municipal, desde Igeltsera hasta Torre Barri y a ambos lados de la calle Goieta hasta el nuevo cementerio.
Algunas acciones concretas a proponer son:

- Mantenimiento de senderos y césped.
- Poda de eucaliptos.
- Trazado y publicidad de recorridos de paseo/senderismo, complementando los ya existentes.

Puesto que la división parcelaria no se corresponde siempre con el uso y entendimiento que los vecinos tienen del suelo,
en algunos casos se requiere información sobre parcelas contiguas para poder asociar parcelas y así simplificar la capa
en los mapas.

Asimismo, puesto que el cordal es el límite natural y parcelario con Sopela, sería recomendable consultar la propiedad
de algunas parcelas del municipio vecino.
En caso de que el Ayuntamiento de Urduliz no pueda consultar la propiedad de dichas parcelas, sería recomendable
contactar con el Ayuntamiento de Sopela (algo que queremos hacer en cualquier caso para idealmente trabajar de forma
conjunta).

Tanto en el caso de las parcelas de Urduliz como las de Sopela, el grupo de trabajo no necesita saber quiénes son los
propietarios, cuestión que puede ser compleja desde el punto de vista del tratamiento de datos personales.
Nos es suficiente con saber cuáles son de propiedad municipal y poder agrupar las contiguas que pertenezcan al mismo
propietario.
Deberá ser el Ayuntamiento (o los Ayuntamientos) quien guarde la información detallada, por si fuera necesaria
información adicional para actuaciones concretas.

Urduliz:

- 089 0001 00001
- 089 0001 00002
- 089 0001 00004
- 089 0001 00005
- 089 0001 00006
- 089 0001 00007
- 089 0001 00008
- 089 0001 00009
- 089 0001 00010
- 089 0001 00011
- 089 0001 00012
- 089 0001 00013
- 089 0001 00014
- 089 0001 00026
- 089 0001 00027
- 089 0001 00028
- 089 0001 00029
- 089 0001 00030
- 089 0001 00031
- 089 0001 00032
- 089 0001 00033
- 089 0001 00034
- 089 0001 00035
- 089 0001 00036
- 089 0001 00037 Baja (supongo que repartido entre 0036 y 0038)
- 089 0001 00038
- 089 0001 00039
- 089 0001 00040
- 089 0001 00041
- 089 0001 00059
- 089 0001 00060
- 089 0001 00061
- 089 0001 00062
- 089 0001 00063
- 089 0001 00064
- 089 0001 00065
- 089 0001 00066
- 089 0001 00078
- 089 0001 00079
- 089 0001 00080
- 089 0001 00081
- 089 0001 00082
- 089 0001 00083
- 089 0001 00084
- 089 0001 00086
- 089 0001 00147
- 089 0001 00148
- 089 0001 00149
- 089 0001 00150
- 089 0001 00151
- 089 0001 00152
- 089 0001 00153
- 089 0001 00154
- 089 0001 00155
- 089 0001 00156
- 089 0001 00157
- 089 0001 00158
- 089 0001 00159
- 089 0001 00160
- 089 0001 00161
- 089 0001 00162
- 089 0001 00163
- 089 0001 00164
- 089 0001 00194
- 089 0001 00200
- 089 0001 00201
- 089 0001 00202
- 089 0001 00213
- 089 0001 00218
- 089 0001 00228
- 089 0001 00229
- 089 0001 00235
- 089 0001 00237
- 089 0001 00242
- 089 0001 00243
- 089 0001 00244
- 089 0001 00245
- 089 0001 00246
- 089 0001 00250
- 089 0001 00252 Baja (supongo que integrado en 0251)
- 089 0001 00253
- 089 0002 00205
- 089 0002 00207
- 089 0007 00001
- 089 0007 00002
- 089 0007 00003
- 089 0007 00004
- 089 0007 00005
- 089 1011 01001
- 089 1011 01002
- 089 1011 02001 ¡Está indicada como Urbana! ¡Puede tener la Fuente de Larragoiti!
- 089 1011 12001 ¡Está indicada como Urbana! ¡Puede tener la Fuente de Larragoiti!
- 089 1011 02022
- 089 1011 02007

Sopela:

- 085 0005 00118
- 085 0005 00121
- 085 0005 00126
- 085 0005 00127
- 085 0005 00130
- 085 0005 00131
- 085 0005 00134
- 085 0005 00135
- 085 0005 00140
- 085 0005 00146
- 085 0005 00147
- 085 0005 00148
- 085 0005 00149
- 085 0005 00150
- 085 0005 00151
- 085 0005 00160
- 085 0005 00162
- 085 0005 00164
- 085 0005 00210
- 085 0005 00211
- 085 0005 00212
- 085 0005 00213
- 085 0005 00215
- 085 0005 00221
- 085 0005 00281
- 085 0005 00430
- 085 1012 02011
- 085 1012 02013
- 085 1012 02014
- 085 1012 02015
- 085 1012 02016
- 085 1012 06001
- 085 1012 06002
