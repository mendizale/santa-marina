# Responsabilidad

- [ESCUELAS DE ESCALADA DE GIPUZKOA: Informe sobre responsabilidad civil y responsabilidad patrimonial de la administración](https://www.gipuzkoa.eus/documents/4004868/4007425/ESKALADA+ESKOLAK-+ERANTZUKIZUN+ZIBILA.pdf), José María Nasarre Sarmiento, 2014 (en el mismo PDF está el documento en euskera, después castellano, y finalmente en inglés)
  - https://www.gipuzkoa.eus/es/web/kirolak/que-hacemos/instalaciones-deportivas/zonas-escalada
  - https://www.gmf.eus/proyecto-de-reequipamiento/

En los croquis (que tienen información y avisos muy pertinentes) se indica qué vías han sido (re)equipadas por la Federación, y cuándo se revisaron por última vez.
