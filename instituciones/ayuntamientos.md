# Ayuntamientos

## Urduliz

```{toctree}
:hidden:

lantaldea/index
```

### Web del ayuntamiento

Actualmente, la [web del Ayuntamiento de Urduliz](https://www.urduliz.eus) no tiene ninguna referencia a la práctica
de la escalada.

Puesto que en el cartel del Parking están los logos de la Diputación, la Federación y el Ayuntamiento, debería haber
al menos una sección en la web del Ayuntamiento con:

- Enlace a la web sobre escalada de la Diputación.
- Enlace a la web de la Federación Vizcaína de Montaña.
- Dirección/teléfono de contacto para reportar incidencias (caídas de bloques, equipación en mal estado o robada).
- Avisos sobre vías cerradas por anidación de aves, terrenos/lindes particulares a respetar y no cruzar.
  O, en su defecto, enlaces a las páginas de la Diputación o de asociaciones ecologistas que faciliten esa información.

Del mismo modo que se enlaza al mapa interactivo del Itinerario de la Memoria, podría enlazarse al que incluye la
ubicación de los sectores de escalada (ver {ref}`mapa`).
De hecho, sería mejor incluir un único enlace a un mapa interactivo que incluya todas las capas (tanto de la memoria
como para senderismo o escalada) que el Ayuntamiento considere adecuadas para representar las alternativas de ocio
que ofrece el cordal.

Adicionalmente, cuando se disponga de {ref}`croquis` actualizados y revisados, deberían incluirse en la página o
remitirse a la Diputación para que actualicen los de su página.
Ver como referencias las *"fichas técnicas"* de las escuelas de gipuzkoa.
Por ejemplo, el de [Olatz Kobalde](https://www.gipuzkoa.eus/documents/4004868/4007425/WEB+OLATZ.pdf/8d738511-4614-e8a2-fe33-42a792e2bed0?t=1679313751548).
También el [decálogo de buenas prácticas](https://www.gmf.eus/galeria/2020/DEKALOGO%202020.pdf).
Y el vídeo [ESKALADA JASANGARRIA Laburmetraia 3 0](https://www.youtube.com/watch?v=cTTuEum0PIg).

A falta de oficina de turismo, estaría bien disponer de copias impresas de los croquis y el decálogo de buenas prácticas
en el polideportivo, en el aula de cultura y/o en el ayuntamiento.

### Carteles

- El cartel/panel sobre escalada de la Diputación, la Federación y el Ayuntamiento tiene un QR para "más información"
  que no funciona.
  - Es muy de agradecer la información en el mismo sobre no abusar de los mosquetones de las reuniones, y sobre cómo
    hacer el cambio para rapelar el último.
  - Se podría completar con información general sobre ¡usar casco! y el civismo en la montaña (limpieza), además de
    especificar qué sectores tienen la base "abajo" y qué sectores tienen la base "expuesta".
  - Ver los folletos técnicos de las escuelas de escalada de Gipuzkoa como referencia.

- Hay un cartel/panel con los croquis de Luis Miguel Equiluz que es difícilmente legible porque el plástico protector se
  ha roto y han entrado agua y hojas.
  Sería muy recomendable retirar ese cartel/panel y poner uno vertical (como el de la Diputación o los del Itinerario de
  la Memoria), con los croquis revisados/actualizados.
  - Una vez más, ver las escuelas de escalada de Gipuzkoa como ejemplo.

- Hay postes señalizando los elementos del Itinerario de la Memoria, pero no indicando los accesos a los sectores de
  escalada.
  Se deberían añadir postes indicando los accesos/senderos a los sectores (algunos coinciden con los senderos del
  Itinerario de la Memoria).

### Cacas (de mascotas y de personas)

*TBC*

### Paredes, senderos y accesos a los sectores

Hay cuatro tipos de tareas a realizar en lo que se refiere a la adecuación y mantenimiento:

- Jardineria desde el suelo.
  Estas tareas pueden realizarlas los mismos trabajadores que mantienen el Parque de la Memoria y otros espacios
  similares en el municipio.
  Es posible que no realicen un desbrozado inicial para eliminar los hasta 2 metros de zarzal que hay en algunos puntos
  concretos, pero una vez retirado en principio pueden mantenerlo.
  El límite de actuación de estos trabajadores pueden ser las parcelas de propiedad municipal.
  Hay que analizar si sería posible que mantuvieran también partes de terreno particular, pero que son de facto de
  disfrute público (como por ejemplo, el tramo entre el Paseo de la Memoria y Moruatxa N Bajo, o el tramo entre el
  Parque de la Memoria y la calle Goieta).

- Jardineria en altura.
  A principio de temporada se debe hacer una revisión y limpieza de las paredes para eliminar algunas plantas concretas
  (las que pinchan) que se encuentren en las vías, para revisar piedras/bloques que hayan podido quedar sueltos tras las
  lluvias otoñales e invernales, y para retirar el musgo que haga peligrosos pasos concretos.
  En principio, esta tarea no requiere el uso de máquinas/herramientas, más allá de unas tijeras de podar y un cepillo.
  Se trata de revisar todas las paredes, pero retirar lo mínimo.
  Sin embargo, require descolgarse de las paredes, por lo que en principio no son tareas que puedan realizar los mismos
  trabajadores que hacen la jardinería en el suelo.

- Instalaciones en el suelo.
  En principio, estas tareas pueden realizarlas los mismos trabajadores que hacen otras instalaciones similares en el
  municipio.
  Hay cuatro tipos de instalaciones a realizar en el suelo:
  - Señalización de (senderos de) acceso a los sectores y renovación de carteles/paneles.
  - Adecuación de las bases de las paredes. Dependiendo del sector:
    - Delimitación y protección de la erosión mediante gravilla. Ver, por ejemplo, {numref}`sendero_delimitado`.
    - Pasamanos o puntos de anclaje para el asegurador.
  - Instalación de placas/chapas para indicar el punto de inicio de cada vía.
  - Escalones con tablones para evitar desprendimiento/erosión en subidas/bajadas como el acceso a Pino Alto o la bajada
    de la cresta al interior de Atxarte; similares a los existentes en Moruatxa N Alto.

- Instalaciones en altura.
  Principalmente lo que se entiende por equipación, ya sean vías de escalada deportiva, travesías o instalaciones tipo
  vía ferrata.
  Además de la equipación y mantenimiento de las vías de escalada deportiva, hay dos puntos en el cordal donde podrían
  hacerse instalaciones tipo vía ferrata (en Erdikoatxa y Atxarte).

Tomando como referencia el modelo de gestión en Gipuzkoa, los trabajos en altura no los realizan trabajadores de empresas
de trabajos verticales.
Se contrata, por mayor o menor duración, a través de la Federación a un grupo de equipadores con experiencia contrastada.
Sería interesante impulsar un modelo similar en Bizkaia.
Mientras tanto, se sobreentiende que los trabajos en altura los realizarán los equipadores y escaladores, como se han
encargado de ello las últimas décadas.
No obstante, mientras se avanza en la infraestructura administrativa para proteger a esas personas en caso de accidente
al realizar esas tareas, sería interesante al menos sufragar los gatos de material.

```{important}
Antes de realizar cualquier actuación, sea en el suelo o en altura, más allá de lo que se está haciendo actualmente,
hay que confirmar dónde existe flora o fauna que deba ser protegida.
Ver {ref}`medio-ambiente`.
```

(sendero_delimitado)=
```{figure} ../_static/img/sendero_delimitado.png

Sendero en Albarracín.
```

#### Actuaciones/tareas concretas

##### Santa Marina NE

Desplazar el cartel/poste y desbrozar el camino principal de acceso a la parte trasera de la Ermita desde el O de la
pared principal (por el depósito).
Actualmente, el cartel está a la altura de la pared de Nabuko, de forma que se pretende que los senderistas y quienes
quieran acceder a los sectores de Moruatxa recorran la base de más de media docena de vías de escalada.
Resulta peligroso que haya gente pasando por donde están los aseguradores de quienes se encuentren escalando esas vías.
Además del riesgo de caída de rocas o del escalador sobre los transeuntes, pueden desconcentrar/molestar al asegurador.

##### Santa Marina Tejado

Una pasarela y/o mirador sobre el tejado de la ermita permitiría protegerla al mismo tiempo que habilitar la práctica de
las escalada y que se pueda disfrutar como un punto desde el que ver todo el pueblo sin necesidad de subir a la Ikurriña,
que puede dar más respeto.
Ver, por ejemplo, {numref}`pasarela_albarracin`.

(pasarela_albarracin)=
```{figure} ../_static/img/Pasarela_Albarracin.jpg

Pasarela en Albarracín.
```

##### Hípica

- Está indicado que hay riesgo de desprendimientos y se recomienda no escalar.
  Sería pertinente analizar cuál es el riesgo real, si es posible retirar los bloques peligrosos, y si no lo es vallar debidamente la pared.
  - Esto es un peligro no sólo para la escalada sino también para actividades/eventos populares que se hacen en el
    Parque de la Memoria.
- El sendero que pasa por encima del sector tiene unos 5 m ligeramente expuestos que pueden ser peligrosos con el
  suelo mojado/resbaladizo.
  Hay dos chapas, una en cada extremo del paso, lo que hace pensar que en su día hubo una cuerda/cable como pasamanos
  que ya no está.
  Sería recomendable poner un cable como pasamanos.

##### Muruatxa N

- Desbrozar los 5 m de zarza/maleza entre el césped cortado del Parque de la Memoria y la base de la pared.

- El acceso a Muruatxa N Alto tenía una cuerda fija que ya no está.
  Actualmente, la gente accede por el otro lado del depósito, que es más tendido y bonito.
  Puede que sea mejor no reponer la cuerda, y en su lugar abrir/limpiar el acceso alternativo.

- En la base de las vías de Muruatxa N Alto hay un cable de acero, pero termina en la base de la última vía (la más al E).
  Para acceder de Muruatxa N Alto hacia el E (y continuar la Arista), no hay protección.
  Hacia el S, hay una reunión que permite hacer un rápel si se realiza la cresta en sentido contrario.
  Hacia el E por la cara N hay tres tornillos, pero no están las chapas/cadena.
  En cualquier caso, los metros desde donde termina el cable hasta las reuniones es expuesta y está desprotegida.
  Sería recomendable añadir un cartel/indicación para evitar que personas menos experimentadas intenten progresar por ahí.
  El camino "natural" para hacer la Arista de O a E es ascender la vía más al O del sector Muruatxa N (Kanala III+).

##### Pino Alto y Arista S

- El sendero para subir de Pino Bajo a Pino Alto es resbaladizo, a pesar de haber una cuerda fija.
  Ese sendero se utiliza para acceder a Pino Alto y también a Arista S.
  Sería recomendable adecuarlo con escalones/tablones de madera.

- En toda la base de Arista S hay un cable de acero, pero en el paso al E para llegar a Pino Alto hay una cuerda.
  Sería recomendable que fuera un cable de acero también.
  La caída es de "sólo" 2-3 m, pero al dar acceso a un sector con 10 vías (por el momento), y permitir hacer la cresta
  sin tramos de escalada (sólo II, sin III+), es de esperar que tenga tránsito.

##### Limpieza de pintadas

Algunos bloques y paredes tienen pintadas de muy escaso valor estético.
Santa Marina N, el bloque de Santa Marina Escaleras, Lukiatxa Nido, etc.

Hay que valorar qué técnica utilizar para desgastar la roca lo mínimo posible, teniendo en cuenta que es muy porosa y al
mismo tiempo frágil.
Dado el impacto que las canteras tuvieron durante el siglo pasado, creo que es relativamente irrelevante el desgaste que
pueda realizarse en comparación con la mejora estética.
Pero aun así se debe tener en cuenta.

AitorU va a preguntar cómo limpiaron las de Araotz.

##### Plantación de árboles y poda

Hay interés en realizar un auzolan para podar los eucaliptos en las parcelas de propiedad municipal, particularmente en
el Haurren Basoa y Lukiatxa.

Por otro lado, se puede y debe podar 5 m alrededor de los monumentos históricos (los nidos de ametralladora y trincheras).

En años anteriores la poda se ha realizado por personal contratado por el Ayuntamiento.

Debemos coordinar los tres factores anteriores para repartir las tareas y asegurarnos de que no quedan zonas sin trabajar
debido a fallos de comunicación.
Sin embargo, lo primero que necesitamos para afrontarlo es la información sobre la propiedad de las parcelas a lo largo
del cordal, pues no tenemos claro cuáles son municipales.

Asimismo, en 2017-2018 se plantaron árboles autóctonos en el esfuerzo de extensión y limpieza de la escuela, y otros
amigos han seguido la tarea.
De forma similar, se realizan actividades con los alumnos de las escuelas de Urduliz, para plantar árboles en el Haurren
Basoa (de ahí el nombre, creo).
Por otro lado, algunos vecinos están, en parcelas de su propiedad, retirando eucalipto y reemplazándolo por especies
autóctonas en sus parcelas.
Sería interesante coordinar estos esfuerzos con los de poda, ya que entiendo que están estrechamente relacionados.

Si hay vecinos dispuestos a reemplazar el eucalipto por bosque autóctono pero no tienen presupuesto para hacerlo, sería
razonable valorar su inclusión en un plan municipal de reforestación.

### Aparcamiento

![](../_static/img/umarcor/Parking.jpeg)

![](../_static/img/umarcor/Parking2.jpeg)

Actualmente, el Parking de Urduliz tiene capacidad para 8-10 vehículos.
A las 7 de la tarde de un domingo de julio, después de varios días de lluvia, había ocho vehículos aparcados y otros dos
en la rampa/pista encima del Parking.
Una semana más tarde, con mejor tiempo, había seis vehículos más.
Sirva para ilustrar que el tamaño actual es muy justo para la afluencia de escaladores y paseantes.
Sería recomendable extender el parking al doble de longitud, ya que parece ser que toda la parcela es del ayuntamiento. Asimismo, sería muy recomendable reducir la profundidad del canal entre la carretera y el parking, ya que es excesiva.
Si no fuera posible estender la longitud por el talud, se podría habilitar la pista/rampa de encima, ya que según el
parcelario es una vía muerta.

```{attention}
Es de vital importancia que el tramo de carretera entre la fuente y el paseo de la Memoria esté libre de vehículos, ya
que por ahí es por donde deben pasar los bomberos en caso de accidente/incendio en cualquier punto entre el parking y el
sector Hípica.

Están pintadas las líneas de amarillo, pero aún así a veces aparcan uno o dos vehículos, que además de causar potencial
peligro molestan al vecino que tiene la entrada/salida a la altura de la fuente.

Sería recomendable mover los bolardos/postes que impiden la circulación por el Paseo de la Memoria varios metros hacia
la fuente, hasta el límite de la entrada del vecino, de forma que no haya sitio para que pueda aparcar un vehículo.
Asimismo, sería recomendable poner un cartel específico indicando que es una vía de emergencia.
```

#### Pernocta

Aunque todavía no es un problema grave, está empezando a serlo.
Este mes de Agosto han pernoctado uno o dos vehículos cada día (que yo haya observado, ninguno más de un día).
La mayoría de personas saben gestionar sus residuos y no se aprecia que han pernoctado.
Pero hay unos pocos que no hacen sus necesidades debidamente.
Conforme crece el número de personas que pernoctan, crecen las heces en términos absolutos.

Hay tres formas de afrontar el problema:

- Habilitar un espacio/zona para hacer necesidades de forma natural/tradicional (usando una hazada de forma que no
  quede rastro).
- Habilitar un servicio, sea químico (como en obras/fiestas) o de hormigón.
- Prohibir la pernocta (y señalizarlo).

Nótese que si se prohíbe la pernocta sin consultarlo/discutirlo con el Ayuntamiento de Sopela, podría tener como
resultado que el problema se traslade al otro lado del cordal.
De hecho, puede que la instalación de un servicio de hormigón tenga más sentido en el Aparcamiento de Sopela, donde
ya hay una base hormigonada y alcantarillado.

Asimismo, nótese que el problema no es exclusivo de quienes pernoctan.
Aunque la mayoría de la gente viene y vuelve a su domicilio, siempre puede haber apretones.
También se realizan otras actividades en el paseo y parque de la memoria.
Conforme se incrementa la afluencia de gente, crece la frecuencia de apretones.

## Sopela

### Aparcamiento

De acuerdo con la señales de tráfico, no es posible acceder al parking de la cara sur de Santa Marina (perteneciente a
Sopela) desde el propio municipio de Sopela.

Hay carretera hasta la parte trasera del Garden Center, pero en los últimos 10-15 m está prohibida la circulación.
Creo que también ha habido una señal de tráfico en el paso de Urduliz a Sopela, pero actualmente no está.

También ha habido un cartel indicando que se trata de un área recreativa por lo que está prohibido aparcar, aunque
actualmente no está el cartel.

Asimismo, por experiencia personal, los municipales de Sopela pueden requerir que se retiren los vehículos de ese
parking por no tratarse de un lugar de estacionamiento.

De facto, se utiliza como aparcamiento porque los municipales rara vez pasan y cuando el aparcamiento de Urduliz está
lleno en este entran hasta una docena de vehículos más.
También es el aparcamiento más cómodo para acceder a Santa Marina S y Moruatxa S.

Sería recomendable hablar con el Ayuntamiento de Sopela para ver si es posible que reconsideren el tratamiento que dan
a esta parcela ({ref}`parcelas:0131`).
