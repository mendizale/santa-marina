#!/usr/bin/env python3

from pathlib import Path
from json import load as json_load, dump as json_dump

ROOT = Path(__file__).resolve().parent

overlays = {}

if (ROOT / 'overlays/raw').exists():

    overlays = {
      'Rios_4326': 'raw/Rios_4326.geojson',
      'Curvas_nivel_4326': 'raw/Curvas_nivel_4326.geojson',
      'Curvas_maestras_4326': 'raw/Curvas_maestras_4326.geojson',
      'Catastro_4326': 'raw/Catastro_4326.geojson',
    }

    with (
        (ROOT / 'overlays/raw/Municipios_4326.geojson').open('r', encoding='utf-8') as rdptr,
        (ROOT / 'overlays' / 'MunicipiosColindantes.geojson').open('w', encoding='utf-8') as wrptr
    ):
        municipios = json_load(rdptr)
        json_dump({
            'type' : municipios['type'],
            'features' : [item for item in municipios['features'] if item['properties']['NOMBRE_TOP'] in [
                'Urduliz',
                'Sopela',
                'Barrika',
                'Berango',
                'Erandio',
                'Laukiz',
                'Gatika',
                'Plentzia'
            ]]
        }, wrptr)

    with (
        (ROOT / 'overlays/raw/Catastro_4326.geojson').open('r', encoding='utf-8') as rdptr,
        (ROOT / 'overlays' / 'ParcelasUrduliz.geojson').open('w', encoding='utf-8') as uwrptr,
        (ROOT / 'overlays' / 'ParcelasSopela.geojson').open('w', encoding='utf-8') as swrptr,
    ):
        catastro = json_load(rdptr)
        json_dump({
            'type' : municipios['type'],
            'features' : [item for item in catastro['features'] if item['properties']['Codigo_Mun'] == 89]
        }, uwrptr)
        json_dump({
            'type' : municipios['type'],
            'features' : [item for item in catastro['features'] if item['properties']['Codigo_Mun'] == 85]
        }, swrptr)

overlays.update({
    'SectoresEscalada': 'SectoresEscalada.geojson',
    'MunicipiosColindantes': 'MunicipiosColindantes.geojson',
    'ParcelasUrduliz': 'ParcelasUrduliz.geojson',
    'ParcelasSopela': 'ParcelasSopela.geojson',
})

content = []

for key, val in overlays.items():
    with (ROOT / 'overlays' / val).open('r', encoding='utf-8') as rdptr:
        content.append(f'const {key} = {rdptr.read()}\n')

with (ROOT / 'public/leaflet/overlays.js').open('w') as wrptr:
    wrptr.write('\n'.join(content))
