# Rocódromos

¿Proyecto rocódromo Uribe Kosta (público, mancomunidad)?

## Urduliz

- Gazteleku (municipal y gratuito)
  - Exterior pero cubierto

### ¿Hormigón proyectado en las paredes del frontón de Marusas?

- https://www.tarrago-rocodromos.es/
  - https://www.tarrago-rocodromos.es/foc_games.html
  - https://www.tarrago-rocodromos.es/fitxa_portugalete.html

## Sopela

- Balea (municipal y gratuito)
  - Descubierto

## Berango

- Sputnik (privado)
  - Cubierto

## Erandio

- Akarlanda (municipal y gratuito) ACTUALMENTE DESMONTADO
  - Descubierto

## Getxo

- Polideportivo Fadura (municipal pero no gratuito)
  - https://www.deia.eus/bizkaia/2014/02/27/getxo-engancha-escalada-5249939.html
  - https://www.getxo.eus/es/servicios/detalle-evento/4326
  - https://ehbildu.eus/herriak/getxo/es/noticias/propuesta-para-crear-un-nuevo-rocodromo-o-boulder-en-getxo

## Leioa

- Boulder Polideportivo Sakoneta (municipal pero no gratuito)
  - https://leioamt.com/el-club/boulder/
  - https://leioamt.com/reapertura-del-boulder-sakoneta/

## Portugalete

- Polideportivo Zubi-Alde (municipal pero no gratuito)
  - Cubierto

- Búlder parque
  - https://www.tarrago-rocodromos.es/fitxa_portugalete.html

- Climbat (privado)

## Barakaldo

- Polideportivo Gorostiza

- Túnel de Ansio hacia Lutxana

- Climbat (privado)

## Bilbao

- Polideportivo El Fango (municipal pero no gratuito)

- Túneles de Txurdinaga (público)

- Piu Gaz (privado)
  - Cubierto

- Búlder paseo San Ignacio

## Etxebarri

## Galdakao

## Derio

- BIWAK (privado)
  - Cubierto
