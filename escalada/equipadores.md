# Equipadores

Nótese que se mencionan aquí las actividades más recientes, aquellos con los que podemos y deberíamos contactar para
confeccionar croquis actualizados y con quienes deberíamos contar para tener información sobre problemas en las paredes
(equipamiento que necesita sustitución, follaje que deba ser retirado, bloques en situación precaria, etc.).

Por lo que tengo entendido, todos ellos han reunido dinero de aficionados utilizando diferentes mecanismos y con ello
han comprado el material con el que se han (re)equipado las vías los últimos 15-30 años.
Naturalmente, hay otros anteriores o parcialmente contemporáneos de los que todavía no tengo conocimimiento o con los
que no he podido contactar.

- Entre 2018 y 2019 **Luis Miguel Eguiluz (LME)** coordinó la reequipación y apertura de nuevos sectores (Muruatxa, Pino o Arista).
  - Algunos de los sectores "equipados por Luismi" tienen material diferente (varillas/chappas o químicos).
    ¿Utilizaron diferente material en 2018-2019 o fue alguien después quien tejió algunas vias con químicos donde sólo
    las había con chapas? ¿Ugaitz?
- **Sherpa** está trabajando en la pared de Nabuko, al O de la pared principal (N) de Santa Marina.
  En el croquis de la Diputación aparecen varias vías marcadas y graduadas pero "Sin nombre".
  Esto es porque los croquis se hicieron sin consultar a Sherpa y, por lo que he entendido, su concepción es que sea una
  especie de rocodromo donde crear/explorar vías, en vez de trazar 3-5 vías concretas.
  Las características de esa pared son muy adecuadas para esa concepción de "puzzle libre".
  También ha abierto alguna vía en el centro de la pared principal, a la altura de Vía Láctea.
  Tiene también alguna vía en el sector Hípica.
  Ha (re)equipado desde hace años (ver {ref}`croquis`).
- **Javi** está (re)equipando algunas vías en Santa Marina N, a la altura de Vía Láctea.
- El sector Moruatxa N Bajo lo han limpiado miembros de Munarrikolanda Mendi Taldea, y **Jon Ander** está equipándolo.
- Hay otro equipador que "se ha metido" donde otros estaban equipando, pero nadie parece tener claro quién es.

## Diputación

De acuerdo con la [página de la Diputación](https://www.bizkaia.eus/es/kirolak/escalada/zona-escalada/-/asset_publisher/Jr1pkxIhZPnM/content/zona-urduliz/880303#com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_Jr1pkxIhZPnMbipo_contenedor_pestanias2):

> En la zona de escalada de Santa Marina, durante el año 2022 se han finalizado los trabajos de revisión y reequipamiento de todas sus vías de escalada existentes. Gracias al equipamiento de varios sectores nuevos durante los últimos años, tenemos más de 100 vías equipadas y repartidas en 8 sectores. Para las que se han utilizado más de 500 anclajes químicos inoxidables y 40 reuniones de anillas.

Puesto que el cartel situado junto a la fuente del parking tiene los logos de la Diputación, la Federación y el Ayuntamiento, entiendo que la equipación la realizó la Federación, no la Diputación.
Sin embargo, no he podido encontrar la memoria/informe que documente qué se cambió y qué se dejó.
La sección [sobre equipamiento](https://bmf-fvm.org/escuela-alta-montana/equipamiento/) de la web de la Federación  sólo tiene memorias de 2011 a 2017.
Sería muy interesante solicitar y disponer de dicho informe.

En Gipuzkoa, la Federación indica en los croquis qué vías han sido reequipadas por ellos y cuándo se revisaron por última vez.
El informe de la Diputación/Federación de Bizkaia del 2022 podría ser un buen punto de partida (junto con la información de los equipadores) para disponer de un registro así en Urduliz.
