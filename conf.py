# -*- coding: utf-8 -*-

import sys, re
from os.path import abspath
from pathlib import Path
from json import dump, loads


ROOT = Path(__file__).resolve().parent


# -- General configuration ------------------------------------------------

numfig = True

extensions = [
    'sphinx.ext.extlinks',
    'sphinx.ext.intersphinx',
#    'sphinxcontrib.bibtex',
    'myst_parser',
    'sphinx_design',
]

myst_enable_extensions = ["colon_fence", "attrs_inline"]

#bibtex_default_style = 'plain'
#bibfiles = [
#    ROOT.parent / 'refs.bib',
#]
#bibtex_bibfiles = [str(item) for item in bibfiles]
#for item in bibfiles:
#    if not item.exists():
#        raise Exception(f"Bibliography file {item} does not exist!")

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

master_doc = 'index'

project = u'Santa Marina'
copyright = u'2023'
author = u'Contributors'

version = "latest"
release = version  # The full version, including alpha/beta/rc tags.

language = 'es'

# reST settings
prologPath = "prolog.inc"
try:
    with open(prologPath, "r") as prologFile:
        rst_prolog = prologFile.read()
except Exception as ex:
    print("[ERROR:] While reading '{0!s}'.".format(prologPath))
    print(ex)
    rst_prolog = ""

# -- Options for HTML output ----------------------------------------------

html_theme = "furo"

html_css_files = [
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/fontawesome.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/solid.min.css",
      "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/brands.min.css",
]

html_theme_options = {
    "source_repository": "https://gitlab.com/urduliz/santa-marina",
    "source_branch": "main",
    "source_directory": "doc/sphinx",
    "sidebar_hide_name": True,
    "footer_icons": [
        {
            "name": "Ayuntamiento de Urduliz",
            "url": "https://urduliz.eus",
            "html": "",
            "class": "fa-solid fa-building-columns",
        },
        {
            "name": "Diputación Foral de Bizkaia | Deporte | Escalada",
            "url": "https://www.bizkaia.eus/es/kirolak/escalada",
            "html": "",
            "class": "fa-solid fa-building",
        },
        {
            "name": "GitLab",
            "url": "https://gitlab.com/urduliz/santa-marina",
            "html": "",
            "class": "fa-solid fa-brands fa-gitlab",
        },
    ],
}

html_static_path = ['_static']

html_extra_path = [str(Path(__file__).resolve().parent / 'leaflet/public')]

html_logo = str(Path(html_static_path[0]) / "img/logo.png")
html_favicon = str(Path(html_static_path[0]) / "img/favicon.png")

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    'papersize': 'a4paper',
}

latex_documents = [
    (master_doc, 'Santa-Marina.tex', u'Santa Marina', author, 'manual'),
]

# -- Options for Texinfo output -------------------------------------------

texinfo_documents = [
  (master_doc, 'Santa-Marina', u'Santa Marina', author, 'Santa-Marina', 'Sport.', 'Miscellaneous'),
]

# -- Sphinx.Ext.InterSphinx -----------------------------------------------

intersphinx_mapping = {
   'python': ('https://docs.python.org/3/', None),
}

# -- Sphinx.Ext.ExtLinks --------------------------------------------------
extlinks = {
   'wikipedia':      ('https://en.wikipedia.org/wiki/%s', 'w:%s'),
   'youtube':        ('https://www.youtube.com/watch?v=%s', 'yt:%s'),
   "web":            ("https://%s", '%s'),
   "gh":             ("https://github.com/%s", 'gh:%s'),
   "gl":             ("https://gitlab.com/%s", 'gl:%s'),
   "glmerge":        ("https://gitlab.com/urduliz/santa-marina/-/merge_requests/%s", '!%s'),
   "glsharp":        ("https://gitlab.com/urduliz/santa-marina/-/issues/%s", '#%s'),
   "glissue":        ("https://gitlab.com/urduliz/santa-marina/-/issues/%s", 'issue #%s'),
   "glsrc":          ("https://gitlab.com/urduliz/santa-marina/-/blob/main/%s", '%s'),
}
