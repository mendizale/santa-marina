# Referencias

- [PLAN GENERAL DE ORDENACION URBANA DE URDULIZ: CRITERIOS, OBJETIVOS Y SOLUCIONES GENERALES DE PLANEAMIENTO. AVANCE](https://www.urduliz.eus/es-ES/Ayuntamiento/Urbanismo/ITA50/apgouc.pdf)

```{note}
Del PGOU se pueden extraer algunas fotografías y mapas interesantes.

Estaría bien preguntar en el Ayuntamiento si hay alguna copia impresa del mismo.
```

- [Urduliz, Auñamendi Eusko Entziklopedia: Fondo Bernardo Estornés Lasa](https://aunamendi.eusko-ikaskuntza.eus/es/urduliz/ar-136695/argazkiak/0/)

## Escuela de escalada

- [thecrag.com/climbing/spain/urduliz](https://www.thecrag.com/climbing/spain/urduliz)
  - 118 vías en 8 sectores (2023/08/06)
- [luismigueleguiluz.blogspot.com](https://luismigueleguiluz.blogspot.com)
  - [Peñas de Urduliz/Sopela](https://luismigueleguiluz.blogspot.com/p/penas-de-urduliz.html)
  - [Nuevas vías en Urduliz](https://luismigueleguiluz.blogspot.com/2020/07/nuevas-vias-en-urduliz.html) 2020 Julio
  - [Peñas de Santa Marina, una escuela viva](https://luismigueleguiluz.blogspot.com/2020/05/penas-de-santa-marina-una-escuela-viva.html?fbclid=IwAR12xPCWRDNr9WBEM_9EV1WE1hIjbGXfvBuSEaktLB3-sA_3r5C4p4TrLJ8) 2020 Mayo
  - [Peñas de Santa Marina](https://luismigueleguiluz.blogspot.com/2019/04/penas-de-santa-marina.html) 2019 Abril
- [LuisMiguelEguiluzAntolin@YouTube](https://www.youtube.com/@LuisMiguelEguiluzAntolin)
  - [Un año entero equipando](https://www.youtube.com/watch?v=3Y3VbcmFKdM)
  - [Cómo poner anclajes quimicos de escalada](https://www.youtube.com/watch?v=qTQiLdfJHh8)
  - [Peligro. Mosquetones desgastados](https://www.youtube.com/watch?v=cnBKBk5Z0i0)
  - [Limpiza de bloques para la seguridad de los escaladores](https://www.youtube.com/watch?v=6N_sGU9uCiw)
- [mendiak.net](https://mendiak.net/)
  - [URDULIZ](https://mendiak.net/viewtopic.php?p=758697) 2020 Septiembre
  - [Restauración y nuevos sectores en Urduliz](https://mendiak.net/viewtopic.php?p=741659) 2018 Junio
  - [Nuevo croquis de Urduliz](https://mendiak.net/viewtopic.php?p=647420) 2014 Octubre
- [flickr.com/photos/hirukaeus: Eskalada Eskola](https://www.flickr.com/photos/hirukaeus/33983804168/in/photostream/)
- Euskalarria (Los Cuatro Gatos)
  - v1 2005 [scribd.com/document/414173470/75052782-EUSKALARRIA-eskalada-gidaliburua-pdf](https://www.scribd.com/document/414173470/75052782-EUSKALARRIA-eskalada-gidaliburua-pdf)
  - v2 2017 [libreriadesnivel.com/libros/euskalarria-20/9788461793129/](https://www.libreriadesnivel.com/libros/euskalarria-20/9788461793129/)
- [hownot2.com/courses](https://www.hownot2.com/courses)
  - [The Bolting Bible](https://www.hownot2.com/post/boltingbible)
  - [Canyon Rope Systems](https://jenksryan.editorx.io/hownot2/post/canyon-rope-systems)
  - [Rope Soloing](https://www.hownot2.com/post/rope-soloing)

## Cinturón de Hierro

- [urduliz.eus](https://www.urduliz.eus)
  - [El Cinturón de Hierro en Urduliz](https://www.urduliz.eus/es-ES/turismo/Lugares-Interes/Paginas/ElCinturondeHierroenUrduliz.aspx)
    - [Tríptico informativo](https://www.urduliz.eus/es-ES/turismo/Lugares-Interes/Cinturon_hierro_info/20200518_triptico_informativo.pdf) PDF 3.83 MB
    - [Mapa interactivo](https://www.google.com/maps/d/u/0/viewer?mid=17aKmSl67AfaC_MdSuRZBm9vzggC2Yolc&ll=43.37449990056062%2C-2.955430999999984&z=16)
    - [Itinerario de la Memoria](https://www.urduliz.eus/es-ES/turismo/Lugares-Interes/Documents/20220321-urdulizko-memoria-ibilbidea.pdf) PDF 8.77 MB
