(cordal)=
# Cordal

De la curva de Torre Barri al nuevo cementerio.

El sendero/camino desde Frailemendi hasta el cruce de Torre Barri es transitable incluso en bici o a caballo.
Sin embargo, el tramo entre el cruce de Torre Barri y el nuevo cementerio no está del todo transitable.

Hay varios vecinos interesados en adecuarlo, y trabajando en ello:

- Tal como se explica en {ref}`parcelas:0153`, el vecino de la primera parcela del cruce de Torre Barri hacia el Este está limpiando el sendero para trabajar el bosque y reemplazar el eucalipto por especies autóctonas.

- Ander, junto con otros vecinos, está limpiando el sendero en sentido contrario: del cementerio nuevo hacia el Oeste.
  El 22 de agosto de 2023 registró el siguiente track con muchas fotografías: [(bridas) cordal de santa marina por Ander 31](https://es.wikiloc.com/rutas-senderismo/bridas-cordal-de-santa-marina-144793838).
  Ver también [gitlab.com/urduliz/santa-marina/-/tree/main/img/ander](https://gitlab.com/urduliz/santa-marina/-/tree/main/img/ander).

## Curva de Torre Barri - Parking de Santa Marina

Aunque no es apto para bicis o caballos, si lo es para senderistas, y no precisa equipación especial (aunque se debe tener
cuidado si se realiza con niños).

Sería interesante desbrozar y habilitar ese tramo, para evitar que los senderistas tengan que bajar hasta el hospital y subir la carretera a Santa Marina, como se muestra en el siguiente track: [Larrabasterra- Frailemendi (108 m)-Santa Marina (177 m)-Lukiatxa (204 m)-Urduliz por haiglo_22](https://www.wikiloc.com/hiking-trails/larrabasterra-frailemendi-108-m-santa-marina-177-m-lukiatxa-204-m-urduliz-121872746).

## Parking de Santa Marina - Moruatxa

Este tramo ha sido muy usado tradicionalmente para subir y bajar a la Ikurriña de Santa Marina, sea por el Oeste (por las escaleras y después por el Oeste de la pared Sur), o por el Este (desde el depósito siguiendo la cresta).

![](../_static/img/ander/SantaMarinaSubidaO.jpg)

Aunque tiene cierta exposición, no precisa equipación.

Se puede evitar por el Paseo de la Memoria, o por un sendero que pasa por el Sur.

## Moruatxa Arista - Lukiatxa

El cordal desde Moruatxa O hasta Moruatxa Arista E es una travesía de escalada/alpinismo con una subida de III/III+, 2-3 travesías de II y un rápel final (independientemente
del sentido en que se haga).
Está equipada, por lo que puede realizarse fácilmente con una cuerda de 30-40 m.
Sin embargo, no es recomendable para senderistas (a pesar de que tradicionalmente muchas personas lo han hecho sin equipación).

Se puede evitar por el Paseo de la Memoria, o por un sendero que pasa por el Sur.

Desde el Este de Moruatxa Arista hasta Lukiatxa hay dos opciones, ligeramente más distanciadas entre si que el tramo anterior,
ya que las parcelas a ambos lados son privadas.

### Por Erdikoatxa

Había un pasamanos sobre el sector Hipica, para pasar del sendero con cuerda que sube de Pino Alto a Moruatxa Arista S, hacia el Este.
Sin embargo, parece que alguien ha quitado la cuerda/cable, ya que sólo queda una chapa a cada lado (en buen estado):

![](../_static/img/umarcor/HipicaPasamanosO.jpeg)

![](../_static/img/umarcor/HipicaPasamanosE.jpeg)

A continuación, se encuentra Erdikoatxa.
Tal como se indica en {ref}`erdikoatxa`, el paso por la cresta es ligeramente expuesto.
Habría que colocar algún cable/pasamanos.
No obstante, de forma similar al Oeste de Atxarte, un cable puede invitar a que haya quien haga el tramo sin la equipación
y técnica adecuada.
Koldo comentó que al planificar el Itinerario de la Memoria pensó en una pasarela (no sólo un cable), pero no sé si llegó
a valorarse debidamente.
Nótese que Koldo es el propietario de la parcela ({ref}`parcelas:0038`) al Norte de Erdikoatxa.

Se puede evitar por el Sur, ya que hay un sendero a 10-20 m de la cresta.
Sin embargo, hay un tramo que es propiedad privada y el vecino ({ref}`parcelas:0281`) parece no estar contento con el
paso de senderistas.

![](../_static/img/ander/ErdikoatxaVallaE.jpg)

### Por la calle Goieta

En vez de pasar por la cresta o por el sur, se puede evitar el tramo por el norte de la hípica, pasando del Parque de la
Memoria a la calle Goieta.
Sin embargo, tal como se expone en {ref}`itinerario`, hay actualmente 3-5 m de maleza ya que la calle Goieta atraviesa
una parcela privada ({ref}`parcelas:0059`).
Suele haber paso abierto por senderistas/vecinos, ya que es un paseo bastante transitado.

El inconveniente de evitar el cordal por la calle Goieta es que no vuelve a haber acceso hasta la subida del Itinerario
de la Memoria, ya que hay 4 parcelas privadas y en cualquier caso la cara Norte no es transitable en ese tramo.

## Lukiatxa - Cementerio

El tramo del Este de Lukiatxa hasta Atxarte no tiene dificultad.
De hecho, es parte de la subida por Haurren Basoa.

Atxarte tiene un paso de 5-10 m que puede requerir equipación.
Tal como se explica en {ref}`atxarte`, puede ser preferible facilitar que se rodee el tramo por el Norte (bajando al
interior de la cantera).

El resto de la cresta sobre la pared Sur de Atxarte es transitable sin mayor dificultad.
El sendero termina pasando por la parte trasera del cementerio, que no esta vallado.
Puesto que el resto de laterales del cementerio están muy vallados, parece haber sido un despiste en el diseño
(desconocimiento de que el cordal fue/es transitable).

![](../_static/img/ander/AtxarteCementerio.jpg)
